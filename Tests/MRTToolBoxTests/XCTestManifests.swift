import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(MRTToolBoxTests.allTests),
    ]
}
#endif
