import XCTest

import MRTToolBoxTests

var tests = [XCTestCaseEntry]()
tests += MRTToolBoxTests.allTests()
XCTMain(tests)
