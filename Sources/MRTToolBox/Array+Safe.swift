//
//  Array+Safe.swift
//  MRTToolbox
//
//  Created by Michael Thomas on 11/9/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation

public extension Array {
    subscript (safe index: UInt) -> Element? {
        return self[safe: Int(index)]
    }
    
    subscript (safe index: Int) -> Element? {
        return index < count ? self[index] : nil
    }
}
