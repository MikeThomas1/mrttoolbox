//
//  TableViewModelDataSource.swift
//  MRTToolbox
//
//  Created by Michael Thomas on 11/9/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit

public class TableViewModelDataSource<ViewModel: TableViewModel, CellType: ConfigurableCell>: NSObject, UITableViewDataSource where CellType.Row == ViewModel.Row  {
    private weak var viewModel: ViewModel?
    
    public init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRowsInSection(section) ?? 0
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections() ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let row = viewModel?.rowAt(indexPath), let cell = tableView.dequeueReusableCell(withIdentifier: CellType.resuseIdentifer, for: indexPath) as? CellType else {
            return UITableViewCell()
        }
        
        cell.configure(with: row)
        
        guard let tableCell = cell as? UITableViewCell else {
            return UITableViewCell()
        }
        
        return tableCell
    }
}
