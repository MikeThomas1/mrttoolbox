//
//  TableViewModel.swift
//  MRTToolbox
//
//  Created by Michael Thomas on 11/9/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation

public protocol TableViewModel: AnyObject {
    associatedtype Row
    associatedtype SectionInfo
    typealias SectionType = SectionItem<SectionInfo, Row>
    
    var sections: [SectionType] { get }
    
    func rowAt(_ indexPath: IndexPath) -> Row?
    func numberOfSections() -> Int
    func numberOfRowsInSection(_ section: Int) -> Int
    func titleForHeaderFooterInSection(_ section: Int) -> SectionInfo?
}

// Since some of these methods are the same for most cases, create a default impl for them
public extension TableViewModel {
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return sections[safe: section]?.rows.count ?? 0
    }
    
    func rowAt(_ indexPath: IndexPath) -> Row? {
        return sections[safe: indexPath.section]?.rows[safe: indexPath.row]
    }
    
    func titleForHeaderFooterInSection(_ section: Int) -> SectionInfo? {
        return sections[safe: section]?.sectionInfo
    }
}
