//
//  SimpleProgressView.swift
//  MRTToolBox
//
//  Created by Michael Thomas on 11/21/20.
//

import SwiftUI

public struct SimpleProgressView: View {
    private let title: String
    public init(_ title: String) {
        self.title = title
    }

    public var body: some View {
        ProgressView(title)
            .padding()
            .background(Color(.systemGray5))
            .cornerRadius(10)
            .foregroundColor(Color(.label))
    }
}

struct SimpleProgressView_Previews: PreviewProvider {
    static var previews: some View {
        SimpleProgressView("Loading...")
//            .preferredColorScheme(.dark)
    }
}
