//
//  Section.swift
//  MRTToolbox
//
//  Created by Michael Thomas on 11/9/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation

public struct SectionItem<SectionModel, Row> {
    public let sectionInfo: SectionModel
    public let rows: [Row]
    
    public init(sectionInfo: SectionModel, rows: [Row]) {
        self.sectionInfo = sectionInfo
        self.rows = rows
    }
}
