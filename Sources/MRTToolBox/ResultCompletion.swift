//
//  Result.swift
//  MRTToolbox
//
//  Created by Michael Thomas on 11/9/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation

public typealias ResultCompletion<Object, Error: Swift.Error> = ((Result<Object, Error>) -> Void)
