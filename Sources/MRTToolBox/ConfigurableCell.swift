//
//  ConfigurableCell.swift
//  MRTToolbox
//
//  Created by Michael Thomas on 1/3/19.
//  Copyright © 2019 Michael Thomas. All rights reserved.
//

import Foundation

public protocol ConfigurableCell {
    associatedtype Row
    
    static var resuseIdentifer: String { get }
    func configure(with viewModel: Row)
}
