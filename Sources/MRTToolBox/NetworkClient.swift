//
//  NetworkClient.swift
//  MRTToolbox
//
//  Created by Michael Thomas on 11/9/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation
import Combine

public class NetworkClient {
    public static let shared = NetworkClient()
    
    private let session = URLSession(configuration: .default)
    private let decodeQueue = DispatchQueue(label: "NetworkClient.Decode.Queue", qos: .userInitiated)
    
    private init() { }
    
    @MainActor
    public func performCodableRequest<ModelType: Codable>(_ urlRequest: URLRequest, decoder: JSONDecoder = JSONDecoder()) async throws -> ModelType {
        let data = try await performDataRequest(urlRequest)
        let model = try await Task(priority: .userInitiated) {
            try decoder.decode(ModelType.self, from: data)
        }.value
        return model
    }
    
    @MainActor
    public func performDataRequest(_ urlRequest: URLRequest) async throws -> Data {
        let (data, response) = try await session.data(for: urlRequest)
        if let httpURLResponse = response as? HTTPURLResponse,
            !(200...300).contains(httpURLResponse.statusCode) {
            throw Error.badResponseCode(httpURLResponse.statusCode)
        }
        return data
    }
    
    public func performCodableRequest<ModelType: Codable>(_ urlRequest: URLRequest, decoder: JSONDecoder = JSONDecoder()) -> AnyPublisher<ModelType, Swift.Error> {
        performDataRequest(urlRequest)
            .receive(on: decodeQueue)
            .decode(type: ModelType.self, decoder: decoder)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    public func performDataRequest(_ urlRequest: URLRequest) -> AnyPublisher<Data, Swift.Error> {
        session.dataTaskPublisher(for: urlRequest)
            .tryMap { (data, response) -> Data in
                if let httpURLResponse = response as? HTTPURLResponse,
                    !(200...300).contains(httpURLResponse.statusCode) {
                    throw Error.badResponseCode(httpURLResponse.statusCode)
                }
                return data
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    public enum Error: Swift.Error {
        case badResponseCode(Int)
        case malformedURL
    }
}
