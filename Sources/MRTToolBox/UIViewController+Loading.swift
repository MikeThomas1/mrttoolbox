//
//  UIViewController+Loading.swift
//  MRTToolBox
//
//  Created by Michael Thomas on 11/21/20.
//

import UIKit
import SwiftUI

public extension UIViewController {
    typealias ProgressViewController = UIHostingController<SimpleProgressView>

    func showLoadingHUD() async -> ProgressViewController {
        view.isUserInteractionEnabled = false

        let progressView = SimpleProgressView("Loading...")
        let vc = UIHostingController(rootView: progressView)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = .clear
        return await withCheckedContinuation { continuation in
            present(vc, animated: true) {
                continuation.resume(returning: vc)
            }
        }
    }

    func hide(loadingHUD hud: ProgressViewController) async {
        view.isUserInteractionEnabled = true

        await withCheckedContinuation { continuation in
            hud.dismiss(animated: true, completion: continuation.resume)
        }
    }
}

