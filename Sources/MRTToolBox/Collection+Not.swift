//
//  Collection+Not.swift
//  MRTToolbox
//
//  Created by Michael Thomas on 1/3/19.
//  Copyright © 2019 Michael Thomas. All rights reserved.
//

import Foundation

public extension Collection {
    var isNotEmpty: Bool {
        return !isEmpty
    }
}
